#include <unistd.h>
#include <sys/time.h>
#include <ruby.h>

/*
 * call-seq: Signal.alarm(time) -> oldtime
 *
 * See alarm(3)
 */
VALUE
sig_alarm(VALUE self, VALUE time)
{
    rb_check_type(time, T_FIXNUM);
    return LONG2FIX((long)alarm(FIX2UINT(time)));
}

/*
 * call-seq: Signal.ualarm(time) -> oldtime
 *
 * See ualarm(3)
 */
VALUE
sig_ualarm(VALUE self, VALUE time)
{
    rb_check_type(time, T_FIXNUM);
    /* TODO check failure */
    return LONG2FIX((long)ualarm(FIX2UINT(time), 0));
}

#define SET_TIMEVAL(s,v) 			\
    (s).tv_sec = floor(v),			\
    (s).tv_usec = fmod(v, 1.0) * 1000000.0

#define GET_TIMEVAL(v) ((v).tv_sec + (double)((v).tv_usec / 1000000.0))

/*
 * call-seq: Signal.setitimer(which, time, inter) -> [oldtime, oldinter]
 *
 * See setitimer(3)
 */
VALUE
sig_setitimer(VALUE self, VALUE which, VALUE time, VALUE inter)
{
    struct itimerval old, new;

    rb_check_type(which, T_FIXNUM);
    rb_check_type(time, T_FLOAT);
    rb_check_type(inter, T_FLOAT);

    SET_TIMEVAL(new.it_value, RFLOAT_VALUE(time));
    SET_TIMEVAL(new.it_interval, RFLOAT_VALUE(inter));

    if (setitimer(FIX2INT(which), &new, &old) != 0) {
	rb_sys_fail("setitimer()");
    }

    return rb_ary_new_from_args(
	2,
	DBL2NUM((double)GET_TIMEVAL(old.it_value)),
	DBL2NUM((double)GET_TIMEVAL(old.it_interval))
    );
}

void
Init_alarm(void)
{
#ifdef HINT_FOR_RDOC
    VALUE mSignal = rb_define_module("Signal");
#endif
    VALUE mSignal = rb_path2class("Signal");

    rb_define_module_function(mSignal, "alarm", sig_alarm, 1);
    rb_define_module_function(mSignal, "ualarm", sig_ualarm, 1);
    rb_define_module_function(mSignal, "setitimer", sig_setitimer, 3);

    /* See getitemer(3) */
    rb_define_const(mSignal, "ITIMER_PROF", INT2FIX(ITIMER_PROF));
    /* See getitemer(3) */
    rb_define_const(mSignal, "ITIMER_REAL", INT2FIX(ITIMER_REAL));
    /* See getitemer(3) */
    rb_define_const(mSignal, "ITIMER_VIRTUAL", INT2FIX(ITIMER_VIRTUAL));
}
