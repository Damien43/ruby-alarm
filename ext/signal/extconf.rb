require 'mkmf'

unless have_func 'alarm', 'unistd.h' and
       have_func 'ualarm', 'unistd.h' and
       have_func 'setitimer', 'sys/time.h' and
       have_func 'getitimer', 'sys/time.h' and
       have_macro 'SIGALRM', 'signal.h'
  abort
end

create_makefile 'signal/alarm'
