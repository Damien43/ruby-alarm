Gem::Specification.new do |s|
  s.name       = 'signal-alarm'
  s.version    = '0.1.0'
  s.summary    = 'alarm() binding'
  s.author     = 'me'
  s.license    = 'GPL'
  s.files      = %w<ext/signal/alarm.c
                    lib/signal/alarm.rb>
  s.extensions = 'ext/signal/extconf.rb'
  s.add_development_dependency 'minitest', '< 42'
end
