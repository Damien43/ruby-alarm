# Provide Signal.alarm() and Signal.setitimer() functions.
#
# See +alarm(3)+
# See +setitimer(3)+

require 'signal/alarm.so'

Signal::Alarm_VERSION = '0.1.0'
