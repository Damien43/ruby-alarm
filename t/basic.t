#!/usr/bin/ruby

require 'minitest/autorun'

require_relative '../lib/signal/alarm.rb'

class TestBasic < Minitest::Test

  def test_1
    assert_raises(TypeError) { Signal.alarm("") }
    assert_instance_of Integer, Signal.alarm(0)
    assert_equal Signal.alarm(0), 0
    Signal.alarm(1)
    p = Signal.alarm(0)
    assert_operator p, :<=, 1
    assert_operator p, :>=, 0
    t = 0
    sighandler = Signal.trap(:ALRM) { t += 1; raise "ZZZ" }
    Signal.alarm(1)
    begin
      sleep 42
    rescue
      Signal.trap(:ALRM, sighandler)
      t += 1
    end
    assert_equal t, 2
  end

  def test_2
    assert_raises(TypeError) { Signal.ualarm("") }
    assert_instance_of Integer, Signal.ualarm(0)
    assert_equal Signal.ualarm(0), 0
    Signal.ualarm(9_999)
    p = Signal.ualarm(0)
    assert_operator p, :<=, 9_999
    assert_operator p, :>=, 1_000
    t = 0
    sighandler = Signal.trap(:ALRM) { t += 1; raise "ZZZ" }
    Signal.ualarm(500)
    begin
      sleep 42
    rescue
      Signal.trap(:ALRM, sighandler)
      t += 1
    end
    assert_equal t, 2
  end

end

# vim:ft=ruby:
